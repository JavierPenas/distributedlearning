## Configuración y puesta en marcha

En esta nueva versión de la aplicación, se han solucionado algunos de los problemas detectados en la anterior, además de aportarse a mayores, la posibilidad de ejecutar la misma en un equipo Windows tradicional, también apoyándonos de Docker.

Las imágenes se encuentran alojadas en:
- registry.gitlab.com/javierpenas/distributedlearning/desktop
- registry.gitlab.com/javierpenas/distributedlearning

Para descargarlas y ejecutarlas, se incluyen dos archivos .yml con lo necesario. Bastará con tener instalado **Docker** y **Docker-compose**, y ejecutar el comando: 
```bash
sudo docker-compose up
```
Esto descargará la imagen, si no existe en el equipo, y arrancará un contenedor.  Si le añadimos la opción -d, el contenedor se ejecutará en segundo plano, y podremos cerrar el terminal desde el cual lo arrancamos:
```bash
sudo docker-compose up -d
```
A mayores, y para poder testear correctamente el funcionamiento, una vez arrancado el contenedor, podemos ver los logs del servidor, ejecutando:
```bash
docker logs -g <ID CONTAINER>
```
Antes de la ejecución de los nuevos contenedores, se recomienda realizar una limpieza del entorno, para liberar almacenamiento consumido de forma innecesaria:
```bash
sudo docker system prune -a
```

## Instalación del entorno Docker en RB
Docker-compose no viene instalado por defecto con la instalación recomendada en Raspberry, por lo que será necesario instalarlo:
##### Docker instalation
```bash
curl -sSL https://get.docker.com | sh
``` 
 ##### Install required packages  
 ```bash
sudo apt update  
sudo apt install -y python python-pip libffi-dev python-backports.ssl-match-hostname 
```
##### Install Docker Compose from pip - This might take a while
 ```bash
sudo pip install docker-compose
```


## Administración de datasets

La gestión de los distintos datasets de entrenamiento disponibles también ha variado ligeramente. 
Ahora, podremos incorporar/eliminar datasets sin necesidad de parar la aplicación.
Los .csv que querramos que estén disponibles deben ubicarse en la ruta
**/home/pi/.data/datasets/train_batches** en el caso de las **Raspberry**. Para el caso del equipo tradicional, el directorio **.data/datasets/train_batches** deberá estar a la misma altura que el docker-compose.yml.
Este directorio se '*copia*' al contenedor en el momento del arranque, por lo que **es importante que exista en el momento de la primera creación** del mismo. Una vez arrancada la aplicación, cualquier .csv que añadamos en el directorio, podrá ser cargado desde la app.

### Organización de los datasets suministrados:
En este mismo proyecto, se proporcionan datasets para el entrenamiento y test de la colección MNIST.
En un primer nivel contamos con **mnist_test** y **mnist_train**, que contienen los conjuntos de entramiento y test al completo. Aunque si recomendamos utilizar el de test para las pruebas en la aplicación, **no se recomienda usar el de entrenamiento al completo**, ya que puede dar problemas por el alto consumo de memoria. Para evitar dichos problemas, se dejan subconjuntos, divididos por cada una de las clases (números del 0 al 9). 
Dentro de las **carpetas asociadas a cada número**, encontraremos **subconjuntos de 1000** datos aproximadamente cada uno.
A su vez, en la carpeta **miniData**, de cada una de las clases, se está trabajando en conjuntos de 250 datos cada uno.